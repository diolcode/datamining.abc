Scripts accompagnés de la documentation utilisateur permettant d'obtenir la matrice *individus-variables* qui servira pour le projet.

//////////////////////////////////////////////////////////////////////////////////////////////////////////

Le script creation_matrice_python ne nécessite que les packages tensorflow,numpy,mysql connector et scikit-learn.
# Objectif 
L'objectif est de pouvoir utiliser cette matrice pour entrainer un réseau neuronal,
 et comparer les résultats du réseau avec un KNN

# Tables utilisées
Il utilise les tables gene,functional_domain,conserved_domain et protein pour la classe.

# Traitement réalisé
Les traitement ici consistent à 
- récupérer le plus d'informations possibles pertinentes sur les tables choisies (ce qui exclue les clés principalement)
- transformer tous les textes récupérés en données numériques (utilisation du TF-IDF principalement)
- normaliser toutes les valeurs numériques entre 0 et 1

## A propos des textes
pré-traiter un texte peut se faire de multiples manières, et ici l'approche choisie est le tf-idf.

En une ligne, il s'agit de considérer un texte comme une matrice de mots,
ou le poids de chaque mot pour définir le texte duquel il est extrait est proportionnel au nombre de fois ou ce mot est présent et inversement 
proportionnel au nombre de fois ou ce mot est présent dans les autres textes.

Cette méthode à l'avantage de conserver une partie de la sémantique d'un texte lors de sa conversion en vecteur.

Cette méthode retourne égalemnt un vecteur comme résultat, et pas simplement une donnée numérique, ce qui veut dire qu'il y a plus de dimensions en sortie qu'en entrée de ce processus.

C'est cette différence de dimension qui nous a poussé a ne pas concatener tous les résultats dans une seule matrice:
- il est possible que les textes aient un poids plus important que les autres données dans la matrice concaténée
- il est probablement plus intéréssant d'avoir un réseau neuronal spécialisé dans les textes

... mais c'est des suppositions en vrai je sais pas.

pour le knn, il est probablement possible de tout concaténer et de ne pas attribuer de poids similaire à toutes les colonnes.

## A propos du nombre de lignes
La matrice générée par ce script comporte actuellement peu de lignes, puisque la requete en base se limite à moins de 10 lignes, afin 
de pouvoir faire tourner le script rapidement.
Le nombre de ligne peut être augmenté en modifiant la denière ligne de la requête "LIMIT X", X étant le nombre de lignes désirées.

Ce chiffre sera augmenté par la suite, l'objectif étant de pouvoir traiter l'intégralité des lignes retournées
par la requête sans stocker le tout en mémoire vive.



