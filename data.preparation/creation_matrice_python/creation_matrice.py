import tensorflow as tf
import mysql.connector
import numpy as np
import sklearn.preprocessing as skp

# import pandas as pd
#connection à la BD
ct = mysql.connector.connect(user="root", password="root", database="fouille", host="127.0.0.1")
cursor = ct.cursor()
query = "SELECT TABLE1.Chromosome,TABLE1.`Start`,TABLE1.`End`,TABLE1.Self_Score,TABLE1.`Function`,TABLE1.Q_Start," \
        "TABLE1.Q_End,TABLE1.S_Start,TABLE1.S_End,TABLE1.Score,TABLE1.Description,p.ABC FROM " \
        "(SELECT g.Chromosome,g.`Start`,g.`End`,g.Self_Score,g.`Function`,c.Q_Start," \
        "c.Q_End,c.S_Start,c.S_End,c.Score,f.Description,g.Gene_ID " \
        "FROM gene g, functional_domain f, conserved_domain c " \
        "WHERE g.Gene_ID=c.Gene_ID " \
        "AND c.FD_ID=f.FD_ID) " \
        "AS TABLE1 LEFT JOIN " \
        "(SELECT protein.`Type` as 'ABC',protein.Gene_ID FROM protein) " \
        "AS p ON TABLE1.Gene_ID=p.Gene_ID " \
        "LIMIT 10;"

nbRow = cursor.execute(query)
predata = cursor.fetchall()  # recuperation de toute la sortie de la requete, pas optimal niveau mémoire,
# fetchmany serait préférable

size = len(predata)          # nombre de lignes retournées par la requete

data = np.fromiter(predata, count=size, dtype=[('Chromosome', 'U7'), ('Start', 'i4'), ('End', 'i4'),
                                               ('Self_Score', 'f4'), ('Function', 'U1455'), ('Q_Start', 'i2'),
                                               ('Q_End', 'i2'), ('S_Start', 'i2'), ('S_End', 'i2'),
                                               ('Score', 'f4'), ('Description', 'U5470'), ('ABC', 'U21')])
#construction d'un tableau structuré à partir du résultat de la requête
# 32Go de mémoire pour tout le jeu de données, si je me trompe pas :p

# le tokenizer decoupe des textes selon les espaces pour faire une liste des mots rencontrés
tokenizer = tf.keras.preprocessing.text.Tokenizer()
tokenizer.fit_on_texts(data['Description'])
# on peut observer les résultats du tokeniser avec
# print(tokenizer.index_word)
# le but de cette manoeuvre est de pouvoir représenter chaque texte sous la forme de
# chiffres (vecteur) pour pouvoir calculer une distance basée sur la sémentique.
# une alternative prometteuse au tf-idf serait l'embedding
# le vectorDescription permet de voir les représentation vectorielles des textes 'Description'
vectorDescription = tokenizer.texts_to_matrix(texts=data['Description'], mode='tfidf')

# pareil pour le champ Function
tokenizer.fit_on_texts(data['Function'])
vectorFunction = tokenizer.texts_to_matrix(texts=data['Function'], mode='tfidf')

# Pareil pour le champ 'Chromosome', mais avec une approche plus simpliste que le tf-idf puisque
# chromosome n'est qu'un mot vide de sens
tokenizer.fit_on_texts(data['Chromosome'])
vectorChromosome = tokenizer.texts_to_matrix(texts=data['Chromosome'], mode='binary')

# Un vecteur booleen indiquant si l'individu est un transporteur ABC ou non (la classe à deviner)
vectorABC = data['ABC'] == 'ABC'

# les données numériques uniquement sont conservées dans realMatrix, puis elles sont normalisées entre 0 et 1
realMatrix = np.reshape(np.concatenate((data['Start'], data['End'], data['Self_Score'], data['Q_Start'],
                                        data['Q_End'], data['S_Start'], data['S_End'], data['Score'])), (size, -1), 'F')
skp.minmax_scale(realMatrix, axis=1, copy=False)

print(realMatrix)
# les vecteurs ne sont pas concaténés à la matrice, puisque chaque colonne de texte est convertie en
# vecteurs à plusieurs dimensions, et auraient de ce fait un opid possiblement plus important que les autres colonnes.
# En plus le texte à peut être besoin d'un apprentissage différent du reste.

