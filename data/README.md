## versions utilisées
- python 3.6.9
- tensorflow-gpu 2.1.0
- mysql-connector-python 

**Répertoire à laisser vide sur gitlab** mais qui localement devrait contenir les fichiers CSV de départ :

- Assembly.tsv.gz
- Chromosome.tsv.gz
- Conserved_Domain.tsv.gz
- Functional_Domain.tsv.gz
- Gene.tsv.gz
- Orthology.tsv.gz
- Protein.tsv.gz
- Strain.tsv.gz
- Taxonomy.tsv.gz
