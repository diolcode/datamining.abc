Pour la remise du projet, un fichier Markdown consitutant le rapport devrait remplacer ce README.



Bien qu'au format markdown, ce sera un vrai rapport de projet.



# Contexte
Dans le cadre de ce projet, on s'intéresse à la mise en oeuvre de la fouille de données dans le contexte des données de la banque publique ABCdb (https://www-abcdb.biotoul.fr/) qui est développée et maintenue à Toulouse. La fouille de données est l'extraction d'un savoir ou d'une connaissnce à partir de grandes quantités de données.Gràce à un ensemble de méthodes avancées et automatiques, on explore et modélise les relations dans ces données. L'objectif du projet est de développer des méthodes efficaces et automatiques pour pouvoir prédire si un gène code ou pas pour un partenaire d’un système ABC. On va aussi  évaluer la qualité de la prédiction effectuée.
Pour celà, nous utilisons les techniques des K plus proches voisins et de réseaux de neurones. Dans un premier temps nous procéderons à l'analyse prélinaire des données où les données intéressantes pour la classification seront extraites dans une matrice pour la suite puis nous étudierons avec python les modèles des K plus proches voisins et de réseaux. Nous évaluerons par la suite la précison de ces modèles sur un gène à codé ou non un système ABC, afin de trouver le meilleur modèle.

![Transporteur ABC](transporteurs.png)

# Analyse

Précisions sur les objectifs à atteindre et comment y arriver. 

Analyse préliminaire des données pour les appréhender ainsi que les méthodes disponibles pour atteindre les objectifs.

L'objectif de ce projet est dans un premier temps de trouver des méthodes pour classifier si un gène code pour un partenaire de système ABC ou non.
Les méthodes retenues on étées le knn et un réseau neuronal.
Le second objectif est de déterminer laquelle de ces méthodes donne de meilleurs résultats.

Pour ce qui est des données, nous avons retenus les tables Chromosomes, functional_domain,gene, conserved_domain et protein pour la classe (ABC ou non).
Parmi ces tables les données considérées arbitraires ou vide de sens ont été omise, telles que les clés primaires. Une jointure à été faite sur toutes ces tables, et un left join a été utilisé sur la table protein de telle sorte que tous les genes soient gardés, même si la colonne type de la table protein ne mentionne pas ABC.

Le choix des tables est influencé par le fait que ce qui caractérise un système ABC est probablement plus dans les tables sélectionnées que dans les tables Taxonomy et strain, les ABC étant présent dans trois règnes du vivant. Les autres tables non utilisées sont des tables expertisées, et les utiliser n'est pas permis.
# Conception

Fort de l'analyse précedente, présenter l'approche choisie et pourquoi. Quelle méthodologie allez-vous mettre en oeuvre pour les différentes étapes du projet avec quelles méthodes (par exemple : obtention de la matrice pour l'analyse, puis quelle méthode de classification et comment se fera l'évaluation). 

Pour la conception, nous avons été encouragés à d'abord faire un éventuel pré traitement des données et les regrouper dans une matrice, avant de réélement procéder à la classification.
La démarche à suivre est donc d'extraire une matrice basée sur les mêmes données, et d'appliquer nos deux méthodes de classification dessus.

# Réalisation

Mise en oeuvre de ce qui a été conçu. Il s'agit la de préciser les paramètres et tous les détails concernant la réalisation concrète de l'analyse. Puis de décrire les résultats obtenus.

## La matrice de données

### La conversion en tableau numpy

Sachant que nous allions probablement utiliser keras et tensorflow pour le réseau neuronal, et que ces frameworks utilisent préférentiellement des entrées de type numpy array, la première chose à faire a été de convertir la sortie de la base de données en tableau numpy.
Cette première étape n'est pas triviale, puisqu'il est nécéssaire de définir un type de données numpy à chaque colonne de la matrice numpy et que chacun de ces types de données corresponde à ce qui est retourné par la base.
Et bien que les types numpy reprennent les types les plus courants (nombres,caractères), ce qui fait l'efficacité d'un tableau numpy est que chaque 'case' d'une colonne d'un tableau numpy fait la même taille. Cette astuce permet une bien plus grande efficacité lors de la lecture/ecriture d'un de ces tableaux.
Il a donc fallu rechercher pour chaque colonne de la base de données sur combien d'octets tient la plus longue valeur de cette colonne.

### Le problème des textes

Une fois cette matrice créée, un second problème se posait : certaines de ces données était des chaines de caractères. Et pour les deux méthodes de classification choisies,il est nécéssaire de pouvoir ordonner les valeurs sur lesquelles on se base pour la classififcation.
Par exemple, supposons deux points A(1) et B(5), A de la classe A et B de la classe B. Si je veux classer un point C(2) en utilisant un knn au premier plus proche voisin, je peux dire que C serait de la classe A. En revanche, si on répète cette expérience mais avec cette fois les points A,B et C situé respectivement à 'café','vache' et 'voiture', il devient nettement plus difficile de classer C. D'ou la question, comment convertir un texte en valeurs numériques ?
Il existe plusieurs méthodes pour arriver à un résultat de ce genre, avec leurs avantages et leurs inconvénients, mais celle qui a été choisie est le tf-idf.


Le tf-idf(term frequency, inverse document frequency), est une méthode qui se base simplement sur les mots d'un texte parmis un ensemble d'autres textes. Pour le calculer, il faut tout d'abord obtenir une liste de tous les mots de tous les textes et compter leurs occurences. Cette occurence est d'abord pondérée par la taille du texte duquel le mot est extrait, ce qui nous donne le term frquency. L'idf est calculé comme étant le logarithme du nombre total de textes divisé par le nombre de textes portant le mot considéré.
La multiplication de ces deux termes donne le tf-idf. Les mots ayant un fort tf-idf sont alors considérés comme représentatifs du texte duquel ils sont extraits.

Cette méthode présente l'avantage de conserver une partie du sens du texte lors de sa conversion en vecteur. Elle ne conserve pas en revanche l'ordre des mots.

Concretement, dans notre cas, nous avons utilisé l'outil Tokenizer de Keras, qui se charge de découper les textes en utilisant espace comme séparateur, et de calculer le le tf-idf.

Une approche moins gourmande a été utilisée pour la colonne portant le nom du chromosome, puisque chaque texte n'est qu'un seul mot, et souvent le même mot.
Il s'agit simplement de créer une nouvelle dimension dans le vecteur représentant le texte pour chaque nouveau mot. Si le mot d'interet correspond à celui qui a servi à créer la colonne, un 1 est inscrit, 0 sinon.

Exemple : textes : [A][A][B] représentations : [1;0][1;0][0;1]

Ces méthodes ont donc permis de transformer les textes en vecteurs exploitables.

### Les autres valeurs

Les valeurs restantes, numériques, ont étées normalisées entre 0 et 1.
Un dernier vecteur booleen a été utilisé pour les classes.

## Le knn
Après examination et extraction de la matrice individus-variables nécessaires pour l'analyse, nous avons jugé d'utliser d'abord la méthode des k plus proche voisins pour la classifiction. c'est une méthode non paramétrique et fait des méthodes les plus simples d’apprentissage supervisé. La méthode KNN est une méthode à base de voisinage, La classe d'une nouvelle est déterminée en fonction de la classe majoritaire  parmi ces k plus proches voisins dans le jeu de données. En effet, pour cette algorithme on  comme comme données d'entrée d'u jeu de données pour l'apprentissage, d'une fonction pour calculer la distance et d'un nombre entier k qui est défini par l'utilisateur. Pour nouvelle observation dont on veut prédire sa classe par exemple, l'algorithme knn procède ainsi :
- Calculer toutes les distances de cette observation  avec les autres observations du jeu de données. 
- Retenir les K observations du jeu de données les proches de l'observation en utilisation le fonction de calcul de distance.
Il existe plusieurs fonctions de calcul de distance, notamment, la distance euclidienne, la distance de Manhattan, la distance de Minkowski, celle de Jaccard, la distance de Hamming…etc. On choisit la fonction de distance en fonction des types de données qu’on manipule. Ainsi pour les données quantitatives et du même type, la distance euclidienne est un bon candidat. Quant à la distance de Manhattan, elle est une bonne mesure à utiliser quand les données ne sont pas du même type. Nous avons utiliser la distnce euclidienne dans ce cas. 
- Donner la classe majoritaire des k observations retenues.
Le  paramètre k doit  être  déterminé  par  l’utilisateur. Le meilleur choix de k dépend du jeu de donnée. En général,  les  grandes  valeurs  de k  réduisent  l’effet  du  bruit  sur  la  classification  et donc le risque de sur-apprentissage,  mais  rendent  les  frontières  entre  classes  moins  distinctes. Un bon k peut être  sélectionné  par  diverses  techniques. Dans notre cas, on fait varier k et pour chaque valeur de k, on calcule le taux d’erreur de l’ensemble de test. On garde le paramètre k qui minimise ce taux d’erreur test.

Par soucis de mémoire, Nous avons utilisé 40000 gènes sur la matrice de données, qui sont divisés en deux parties. 20000 oservations pour l'échantillon d'apprentissage et 20000 pour le jeu de test. Après les tests, nous avous calculer la moyenne du taux d'erreurs et on peut voir que le modèle a un tzux d'erreurs de 25,7%. La performance est environ 96% et la précision est de 97%. Après avoir varier k de 2 à 20, on a appercu que le nombre de k le plus optimal est 6.

## Le réseau neuronal

### Architecture générale
Pour le réseau neuronal, nous avons choisis de faire un réseau à quatres entrées, une pour chaque vecteur de données que nous avions :les données numériques, mais également les textes convertis en vecteur.
La question de faire plusieurs entrées s'est posée, et comme il est possible que chaque texte ait besoin d'un apprentissage différent, nous avons optés pour plusieurs entrées. Après être traitées ces entrées sont sont concaténées dans une même couche avant de donner la classification dans un seul noeud de sortie.

![Structure du reseau](my_first_model_with_shape_info.png)

### Architecture détaillée

Chaque layer est dense, c'est à dire que chaque neurone d'une couche est connectée à chaque neurone de la couche suivante et précédente.
Ce n'est pas un choix d'architecture très avancé, puisqu'il existe d'autres architectures, telles que les réseaux convolutif, qui arrivent avec beaucoup de données en entrée à faire ressortir certaines informations essentielles en utilisant moins de calcul qu'un réseau dense.
Néanmoins, le réseau dense est considéré polyvalent, et est généralement un bon point de départ.

La fonction d'activation choisie pour la plupart des layers est la fonction Rectified linear unit ou relu. Il s'agit d'une fonction similaire à une fonction linéaire pour x>0, mais où y=0 si x<=0. Cette fonction est celle proposée par défaut parcequ'elle ne sature pas aussi rapidement qu'une sigmoide quand on s'écarte de 0, ce qui rend l'apprentissage dans les réseaux profonds plus simple en diminuant le problème dit du 'vanishing gradient'.
Ce problème fait que les neurones les plus profondément enfouies dans le réseau de neurones ne subissent pas ou très peu  de rectification dans leurs activation lors d'un apprentissage.
Ce soucis ne devrait pas nous concerner dans notre modèle cela dit, celui ci étant relativement simple.

![fonction relu](440px-Rectifier_and_softplus_functions.svg.png)

Le seul layer n'ayant pas relu comme fonction d'activation est le tout dernier, qui utilise la fonction sigmoide. Comme dit plus haut la sigmoide sature très rapidement autour de 0, puisque la fonction à une forme de 'S' centré en 0, ayant une asymptote en 0 pour x->-inf et une asymptote en 1 pour x-> +inf.

![fonction sigmoide](440px-SigmoidFunction.svg.png)

Ceci est volontaire afin que la sortie soit un résultat tranché entre 1 - classifié comme ABC et 0 - classifié comme non ABC

### A propos du jeu de données

Le jeu de données, la matrice obtenue précédemment a été séparée en jeu de test et en jeu d'entrainement en utilisant la fonction train_test_split de sklearn, de telle sorte que la moitié des données soit utilisée comme test. Le tirage est aléatoire, mais en utilisant une seed pour la reproductibiblité.

En outre, il est devenu rapidement évident qu'utiliser le tf-idf avait tendance a faire exploser le nombre de paramètres en entrée de notre réseau, puisque la colonne description contient de longues phrases.
Le nombre de mots considérés pour l'entrée de description a donc été limité à 128, les 128 mots ayant le tf-idf le plus important. 

Finalement, pour le réseau neuronal, le jeu de données est extrait et traité à la volée à partir de la base et n'est jamais écrit sur le disque en intermédiaire.
Ceci est du au fait que j'aurais souhaité pouvoir traiter toute la base par morceau à la volée, mais ca n'a pas été fait finalement. 

### résultats

Voici les résultats obtenus sur 40000 lignes, dont 20000 sont utilisées pour l'entrainement et 20000 autres pour les tests:
![loss_accuracy](loss_accuracy_40000.png)

la courbe rouge représente la fonction de perte, et la fonction bleue la fonction accuracy, le tout en fonction des epochs, 10 au total.
Comme il peut être difficile de distinguer les valeurs de ces différents points, voici l'équivalent numérique:

'loss': [0.10996165065318346, 0.023310552082583307, 0.01772319363132119, 0.014936089174635708, 0.01288886235337704
4, 0.01113589035226032, 0.00976854318846017, 0.00947425842997618, 0.008804447880201042, 0.0075519388141809035], 

'binary_accuracy': [0.96755, 0.9945, 0.996, 0.99655, 0.9968, 0.9967, 0.99685, 0.997, 0.99725, 0.99735]}

La taille d'un batch pour l'apprentissage étant réglée à 64 dans ce cas.

Une epoch est une itération sur l'ensemble du jeu d'apprentissage, un batch est combien de lignes sont données successivement au jeu de données avant de ne faire tourner l'algorithme d'optimisation (adam dans ce cas), l'algorithme qui permet véritablement au réseau d'apprendre.

La fonction loss est une fonction qui représente l'écart au résultat, ou l'erreur et doit donc etre la plus basse possible.
La fonction Accuracy mesure au contraire la proportion de bonne réponse et doit tendre vers 1. Comme nous n'avons normalement que deux classes en sortie, et que notre réseau sort un résultat entre 0 et 1 (pouvant donc être 0.8), la fonction accuracy arrondit au plus proche.

En voyant ces résultats qui semblent prometteurs, on pourrait se demander si notre réseau n'est pas en train d' 'overfit', c'est à dire apprendre par coeur le jeu de données de test.
Pour cela voici le résultat du test :

![test](test_40000.png)

Malgré des résultats moins bon sur le test qu'en apprentissage, notre réseau fonctionne correctemment.

# Discussion

Analyse et discussion sur les résultats obtenus. 
Conclusions sur la qualité de la ou des méhtodes mises en oeuvre.

Avec le knn on note une performance satisfaisante (96%) avec 6 voisins. Cependant, les performances dépendent du jeu d'appendrentissage. on pouvait obtenir peut être donc d’autres résultats différents en élargissant le jeu de données.
En gros, vue les résultats on peut dire que le modèle est capable de faire de bonnes prédictions. Mais ces bonnes performances sur le jeu de données qu'on a ne garantissent pas que le modèle serait capable de généraliser. Et du fait des contraintes de temps de calcul et des ressources en mémoire, on n'a pu tester l'efficacité du modèle knn pour des jeux de données larges.

En conclusion, les deux méthodes donnent des résultats corrects (96% de précision pour le knn, contre 99,7% pour le réseau), mais le réseau de neurone fonctionne légérement mieux.

# Bilan et perspectives

Qu'est-ce qui fonctionne ou pas. Piste d'amélioration. Recul sur l'ensemble du projet. Si c'était à refaire...

Le point noir de ce projet reste la gestion de la mémoire qui n'a pas encore été résolue. Il serait possible en python d'utiliser un générateur plutot que des structures de données statiques pour améliorer les performances.


performance mémoire pour le réseau neuronal pour 10000 lignes
![memoire](memory_usage.png)

performance mémoire pour le réseau neuronal pour 40000 lignes
![memoire](memory_usage_40000.png)
En outre, pour la création de la matrice le traitement du texte aurait pu être mieux géré, comme par exemple en supprimant les caractères vide de sens comme '.', ou en utilisant eventuellement juste une pseudo-racine des mots (stemming) au lieu des mots complets, bien que cette approche doit être limitée lorsque le vocabulaire employé est très technique.

Mais de manière générale, les deux méthodes fonctionnent correctement.
# Gestion du projet

Comment s’est organisé le groupe. Comment se sont déroulées les discussions, les prises de décisions. Comment se sont répartities les tâches. Quels ont été les rôles et les contributions de chacun·e. Diagramme de Gantt avec le calendrier et les tâches.

Pour l'organisation, nous avons ensemble discuté des tables à garder sur la matrice, puis nous nous sommes partagés les deux méthodes à coder avant de mettre en commun.
