#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~ Importation des packages requis
import argparse
import csv
import numpy as np
import scipy.stats
import sys
from pprint import pprint
import pandas as pd
from sklearn import neighbors
from sklearn.metrics import classification_report
import matplotlib.pyplot as plt


# ~ Le jeu de données utilisé se compose de 8 variables explicatives quantitatives et 1 variable binaire à prédire (1 pour un gène ABC et 0 pour un gène non ABC).
# ~ On dispose de 20000 individus d’apprentissage et 20000 individus pour le test.

# ~ 1. Les parmètres du script
parser = argparse.ArgumentParser(description='k-nn classifier.')
parser.add_argument('-t', '--training', required=True, help='File containing training examples (class,att1,...')
parser.add_argument('-u', '--test', required=False, help='File containing test data to estimate performances.')
parser.add_argument('-k', '--k', required=False, default=5, help='Number of nearest neighbor to consider (default 5)', type=int)
parser.add_argument('-n', '--normalize', required=False, action="store_true", help='Normalize values.')
parser.add_argument('-v', '--verbose', required=False, action="store_true", help='Verbose output.')
 
opt = parser.parse_args()


# AFFICHAGE DES PARAMETRES PASSES EN LIGNE DE COMMANDE
opt = parser.parse_args()
if opt.verbose:
	print(opt)
	
# ~ 2. Chargement du jeu de d'apprentissage avec le module csv (fichier csv)
vectorABC = []
data = []
with open(opt.training) as csvfile: # open the specified file
	csvreader = csv.reader(csvfile, delimiter=',')  # instantiate CSV
	head = next(csvreader) # the first line contains the column names
	if opt.verbose:
		print("header:", head)
	for row in csvreader:
		vectorABC.append( row.pop(0) ) # the first column contains the class
		data.append( row )


# ~ 3. Classificateur
# ~ Transformtion du jeu de données en matrice		
xtrain = np.array(data).astype(np.float) # convertir données  en tableau de float

# ~ Normalisation des données
if opt.normalize :
	means = np.mean(xtrain, axis = 0)
	stds = np.std(xtrain, axis = 0)
	xtrain = (xtrain - means)/stds
 
# INSTANTIATE CLASSIFIER
# ~ On définit le modèle de classification supervisée ‘knn’ avec le package “sklearn”, 
knn = neighbors.KNeighborsClassifier( opt.k )
knn.fit(xtrain, vectorABC) # On l'entraine

# ~ Création d'un individu et utilisation du classificateur :
# ~ On applique maintenant la fonction ‘knn’ du package avec k=5 voisins pour prédire la classes d'un individu test :
sample = [1.00000000e+00, 9.98109970e-01, 1.30008346e-03, 0.00000000e+00, 1.55973319e-04, 2.75247034e-05, 1.83498023e-04, 1.37623517e-04] 
print(knn.predict([sample]))	

# ~ 4. Evalution des performances

# ~ Chargement du jeu de test
ytest = [] # colonne de la classe
xtest = []
with open(opt.test) as csvfile: # open the specified file
	csvreader = csv.reader(csvfile, delimiter=',')  # instantiate CSV reader
	head = next(csvreader) # the first line contains the column names
	if opt.verbose:
		print("header:", head)
	for row in csvreader:
		ytest.append( row.pop(0) ) # the first column contains the class
		xtest.append( row )


# transformation du jeu de test en matrice
T = np.array(xtest).astype(np.float)

# ~ Normalisation
if opt.normalize :
	T  = (T - means)/stds
	
ypred = (knn.predict(T))

	
# ~ Calcul calcul du taux d’erreur
errors = 1 - knn.score(calcul du taux d’erreur de l’ensemble test kT, ytest)
print('Erreur: %f' % errors)
# ~ ou
error = np.sum(ytest!= ypred)/len(ytest)
print(error)

print(classification_report(ytest, ypred))

# ~ Détermination de la meilleure valeur de k 

errors = []
for k in range(2,20):
    knn = neighbors.KNeighborsClassifier(k)
    errors.append(100*(1 - knn.fit(xtrain, vectorABC).score(T, ytest)))
plt.plot(range(2,20), errors, 'o-')
plt.show()
# on voit que 6 est le meilleur k.
