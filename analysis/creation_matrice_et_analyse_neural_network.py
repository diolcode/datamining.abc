import tensorflow as tf
import mysql.connector
import numpy as np
import sklearn.preprocessing as skp
from sklearn.model_selection import train_test_split
import time
import matplotlib.pyplot as plt


# import pandas as pd
# connection à la BD
start_time = time.time()

ct = mysql.connector.connect(user="root", password="root", database="fouille", host="127.0.0.1")
cursor = ct.cursor()
query = "SELECT TABLE1.Chromosome,TABLE1.`Start`,TABLE1.`End`,TABLE1.Self_Score,TABLE1.`Function`,TABLE1.Q_Start," \
        "TABLE1.Q_End,TABLE1.S_Start,TABLE1.S_End,TABLE1.Score,TABLE1.Description,p.ABC FROM " \
        "(SELECT g.Chromosome,g.`Start`,g.`End`,g.Self_Score,g.`Function`,c.Q_Start," \
        "c.Q_End,c.S_Start,c.S_End,c.Score,f.Description,g.Gene_ID " \
        "FROM gene g, functional_domain f, conserved_domain c " \
        "WHERE g.Gene_ID=c.Gene_ID " \
        "AND c.FD_ID=f.FD_ID) " \
        "AS TABLE1 LEFT JOIN " \
        "(SELECT protein.`Type` as 'ABC',protein.Gene_ID FROM protein) " \
        "AS p ON TABLE1.Gene_ID=p.Gene_ID " \
        "LIMIT 40000;"

nbRow = cursor.execute(query)
predata = cursor.fetchall()  # recuperation de toute la sortie de la requete, pas optimal niveau mémoire,
# fetchmany serait préférable

size = len(predata)  # nombre de lignes retournées par la requete

data = np.fromiter(predata, count=size, dtype=[('Chromosome', 'U7'), ('Start', 'i4'), ('End', 'i4'),
                                               ('Self_Score', 'f4'), ('Function', 'U1455'), ('Q_Start', 'i2'),
                                               ('Q_End', 'i2'), ('S_Start', 'i2'), ('S_End', 'i2'),
                                               ('Score', 'f4'), ('Description', 'U5470'), ('ABC', 'U21')])
# construction d'un tableau structuré à partir du résultat de la requête
# 32Go de mémoire pour tout le jeu de données, si je me trompe pas :p

# le tokenizer decoupe des textes selon les espaces pour faire une liste des mots rencontrés
tokenizerDescription = tf.keras.preprocessing.text.Tokenizer(num_words=128)
tokenizerFunction = tf.keras.preprocessing.text.Tokenizer()
tokenizerChromosome = tf.keras.preprocessing.text.Tokenizer()
tokenizerDescription.fit_on_texts(data['Description'])
# on peut observer les résultats du tokeniser avec
# print(tokenizer.index_word)
# le but de cette manoeuvre est de pouvoir représenter chaque texte sous la forme de
# chiffres (vecteur) pour pouvoir calculer une distance basée sur la sémentique.
# une alternative prometteuse au tf-idf serait l'embedding
# le vectorDescription permet de voir les représentation vectorielles des textes 'Description'
vectorDescription = tokenizerDescription.texts_to_matrix(texts=data['Description'], mode='tfidf')

# pareil pour le champ Function
tokenizerFunction.fit_on_texts(data['Function'])
vectorFunction = tokenizerFunction.texts_to_matrix(texts=data['Function'], mode='tfidf')

# Pareil pour le champ 'Chromosome', mais avec une approche plus simpliste que le tf-idf puisque
# chromosome n'est qu'un mot vide de sens
tokenizerChromosome.fit_on_texts(data['Chromosome'])
vectorChromosome = tokenizerChromosome.texts_to_matrix(texts=data['Chromosome'], mode='binary')

# Un vecteur booleen indiquant si l'individu est un transporteur ABC ou non (la classe à deviner)
vectorABC = data['ABC'] == 'ABC'

# les données numériques uniquement sont conservées dans realMatrix, puis elles sont normalisées entre 0 et 1
realMatrix = np.reshape(np.concatenate((data['Start'], data['End'], data['Self_Score'], data['Q_Start'],
                                        data['Q_End'], data['S_Start'], data['S_End'], data['Score'])), (size, -1), 'F')
skp.minmax_scale(realMatrix, axis=1, copy=False)

# print('realMatrix :')
# print(realMatrix)
# les vecteurs ne sont pas concaténés à la matrice, puisque chaque colonne de texte est convertie en
# vecteurs à plusieurs dimensions, et auraient de ce fait un poid possiblement plus important que les autres colonnes.
# En plus le texte à peut être besoin d'un apprentissage différent du reste.

time_preprocessing = time.time()-start_time
print('preprocessing time : ', time_preprocessing)

# ///////////////////////////////

Description_train, Description_test, Function_train, Function_test, Chromosome_train, Chromosome_test, RM_train, RM_test, y_train, y_test = \
    train_test_split(vectorDescription, vectorFunction, vectorChromosome,
                     realMatrix, vectorABC, test_size=0.5, random_state=42)

Description_input = tf.keras.Input(shape=len(vectorDescription[0]),name='Description')
Function_input = tf.keras.Input(shape=len(vectorFunction[0]),name='Function')
Chromosome_input = tf.keras.Input(shape=len(vectorChromosome[0]),name='Chromosome')
RM_input = tf.keras.Input(shape=len(realMatrix[0]), name='RM')

dense1_description = tf.keras.layers.Dense(128, activation="relu")
xd1 = dense1_description(Description_input)
dense2_description = tf.keras.layers.Dense(64, activation="relu")
xd2 = dense2_description(xd1)
dense3_description = tf.keras.layers.Dense(8, activation="relu")
xd3 = dense3_description(xd2)

dense1_function = tf.keras.layers.Dense(16, activation="relu")
xf1 = dense1_function(Function_input)
dense2_function = tf.keras.layers.Dense(8, activation="relu")
xf2 = dense2_function(xf1)

dense1_chromosome = tf.keras.layers.Dense(8, activation="relu")
xc1 = dense1_chromosome(Chromosome_input)
dense2_chromosome = tf.keras.layers.Dense(8, activation="relu")
xc2 = dense2_chromosome(xc1)

dense1_RM = tf.keras.layers.Dense(8, activation="relu")
xr1 = dense1_RM(RM_input)
dense2_RM = tf.keras.layers.Dense(8, activation="relu")
xr2 = dense2_RM(xr1)

x_concat = tf.keras.layers.concatenate([xd3, xf2, xc2, xr2])
abc_pred = tf.keras.layers.Dense(1, name='abc', activation='sigmoid')(x_concat)

nb_epochs = 10;

model = tf.keras.Model(
    inputs=[Description_input, Function_input, Chromosome_input, RM_input],
    outputs=[abc_pred],
)

model.compile(
    loss=tf.keras.losses.BinaryCrossentropy(),
    optimizer=tf.keras.optimizers.Adam(),
    metrics=[tf.keras.metrics.BinaryAccuracy()],
)

history = model.fit(
    {"Description": Description_train, "Function": Function_train, "Chromosome": Chromosome_train, "RM": RM_train},
    {"abc": y_train},
    epochs=nb_epochs,
    batch_size=64,
)
print("history:")
print(history.history)

test_scores = model.evaluate(
    {"Description": Description_test, "Function": Function_test, "Chromosome": Chromosome_test, "RM":RM_test},
    {"abc": y_test},
    verbose=2
)
print("Test loss:", test_scores[0])
print("Test accuracy:", test_scores[1])

print('total_time :', time.time()-time_preprocessing)
# tf.keras.utils.plot_model(model, "my_first_model_with_shape_info.png", show_shapes=True)
plt.plot(range(0, nb_epochs), history.history['loss'],'r--', range(0,nb_epochs), history.history['binary_accuracy'],'bs')
plt.show()

